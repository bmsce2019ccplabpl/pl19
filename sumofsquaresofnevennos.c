#include <stdio.h>

int main()
{
    int i,n;
    int sum=0;
	printf("Enter the value of n:");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
            sum+=(2*i)*(2*i);
    }
    printf("Sum of squares of first %d even natural numbers=%d\n",n,sum);
	return 0;
}
