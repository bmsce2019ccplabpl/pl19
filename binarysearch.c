#include <stdio.h>

int main()
{
    int  beg=0,n,a[10],i,key,found=0;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    int end=n-1,mid;
    printf("Enter the element you want to search\n");
    scanf("%d",&key);
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(a[mid]==key)
        {
            found=1;
            break;
        }
        else if(a[mid]<key)
        {
            beg=mid+1;
        }
        else
        {
            end=mid-1;
        }
    }
    if(found==1)
    {
        printf("Element is found at %d position\n",mid+1);
    }
    else
    {
        printf("Element is not found\n");
    }
    return 0;
}
