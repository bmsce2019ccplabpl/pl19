#include <stdio.h>

int main()
{
    int a[10],n,element,found=0,pos,i,j;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the element you want to search\n");
    scanf("%d",&element);
    for(j=0;j<n;j++)
    {
        if(a[j]==element)
        {
            found=1;
            pos=j;
        }
    }
    if(found==1)
    {
        printf("Element is found at %d position\n",pos+1);
    }
    else
    {
        printf("Element is not found\n");
    }
    return 0;
}
