#include <stdio.h>

int main()
{
    int a[10],n,i,element,j,k,pos;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the element to be inserted in the array\n");
    scanf("%d",&element);
    printf("Enter the position at which the element is to be inserted\n");
    scanf("%d",&pos);
    for(j=n-1;j>=pos-1;j--)
    {
        a[j+1]=a[j];
    }
    a[pos-1]=element;
    n=n+1;
    printf("The array after insertion of the element is");
    for(k=0;k<n;k++)
    {
        printf("%d\t",a[k]);
    }
    return 0;
}