#include <stdio.h>

int main()
{
    int n,i=1,sum=0;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    while(i<=n)
    {
        sum=sum+i;
        i++;
    }
    printf("Sum of %d natural numbers is %d\n",n,sum);
	return 0;
}
