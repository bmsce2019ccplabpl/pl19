#include <stdio.h>
#include <math.h>
int main()
{
    float x1,x2,y1,y2,dist;
	printf("Enter the coordinates of point 1\n");
    scanf("%f%f",&x1,&y1);
    printf("Enter the coordinates of point 2\n");
    scanf("%f%f",&x2,&y2);
    dist=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
    printf("Distance between the given points 1 and 2 is %f",dist);
	return 0;
}
