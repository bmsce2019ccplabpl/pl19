#include <stdio.h>
void swap(int*a,int*b);
int main()
{
    int x,y;
    printf("enter the values of x and y\n");
    scanf("%d%d",&x,&y);
    printf("Before swapping \n x=%d \n y=%d \n",x,y);
    swap(&x,&y);
    printf("After swapping\n x=%d\ny=%d\n",x,y);
    return 0;
    
    
}
void swap(int*a,int*b)
{
    int m;
    m=*a;
    *a=*b;
    *b=m;
}