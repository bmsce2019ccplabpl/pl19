#include <stdio.h>

int main()
{
    int i,n,a[20],s_pos=0,l_pos=0,temp=0,small,large;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    small=a[0];
    large=a[0];
    for(i=1;i<n;i++)
    {
        if(a[i]<small)
        {
            small=a[i];
            s_pos=i;
        }
        if(a[i]>large)
        {
            large=a[i];
            l_pos=i;
        }
    }
    printf("Largest value=%d\nSmallest value=%d\n",large,small);
    temp=a[l_pos];
    a[l_pos]=a[s_pos];
    a[s_pos]=temp;
    printf("The elements of the array after interchanging the largest and smallest values are\n");
    for(i=0;i<n;i++)
    {
        printf("%d\n",a[i]);
    }
	return 0;
}