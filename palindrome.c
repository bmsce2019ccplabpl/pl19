#include <stdio.h>

int main()
{
    int n,rev=0,rem,pal;
	printf("Enter a number\n");
    scanf("%d",&n);
    pal=n;
    while(n!=0)
    {
        rem=n%10;
        rev=rev*10+rem;
        n=n/10;
    }
    if(rev==pal)
    {
        printf("The number is a palindrome\n");
    }
    else
    {
        printf("The number is not a palindrome\n");
    }
    return 0;
}

