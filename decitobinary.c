#include <stdio.h>
#include <math.h>
int main()
{
    int dec,rem,binary=0,i=0;
    printf("Enter a decimal number:");
    scanf("%d",&dec);
    while(dec!=0)
    {
        rem=dec%2;
        binary+=rem*pow(10,i);
        dec=dec/2;
        i++;
    }
    printf("The binary number of the given decimal number=%d\n",binary);
	return 0;
}
